<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/** @var \Composer\Autoload\ClassLoader $loader */
$loader = require __DIR__ . '/../app/autoload.php';
$request = Request::createFromGlobals();

/**
 * Simple function add patient to doctor
 * @param Request $request
 * @return JsonResponse
 */
function addPatientToDoctor(Request $request) {
    
    $doctorId = $request->get('idDoctor');
    $patientId = $request->get('idPatient');

    $doctorRepository = new \AppBundle\Repository\DoctorRepository();
    $patientRepository = new \AppBundle\Repository\PatientRepository();

    $doctor = $doctorRepository->selectById($doctorId);
    $patient = $patientRepository->selectById($patientId);
 
    if (!$doctor || !$patient) {
        return new JsonResponse(array(
            'msg' => 'Missing required data.'
        ));
    }

    $doctor->addPatient($patient);

    return new JsonResponse(array(
        'doctor' => $doctor,
        'patients' => $doctor->getPatients(),
    ));
}

echo addPatientToDoctor($request);
