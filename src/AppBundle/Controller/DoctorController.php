<?php

namespace AppBundle\Controller;

use AppBundle\Form\UserType;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Doctor;

/**
* DoctorController is a sample controller class ...
*
* @package  AppBundle
* @author   Piotr Curyllo <piotr.curyllo@itligent.pl>
* @version  1.0
*/
class DoctorController extends Controller {

     /**
     * Presents doctor form
     * @param Request $request
     * @access public
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function newAction(Request $request) {
        // implements show new doctor form
        // ...
    }
    
    /**
     * Simple method to save doctor/patient data.
     * @param Request $request
     * @param Doctor $doctor
     * @access public
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function saveAction(Request $request, Doctor $doctor = null) {

        // if $doctor is null => save new record else modify existing data.
        if ($doctor == null) {
            $doctor = new Doctor();
        }
        
        // 1) build form
        $form = $this->createForm(new DoctorType(), $doctor);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            
            // 2) save data
            $em = $this->getDoctrine()->getManager();
            $em->persist($doctor);
            $em->flush();
            
            // 3) preparing response
            return new \Symfony\Component\HttpFoundation\JsonResponse(array(
                'doctor' => $doctor,
                'patients' => $doctor->getPatients(),
            ));
        }

        // TODO: add ajax validating form
    }

}
