<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class DoctorType extends AbstractType {

    public function buildForm(FormBuilderInterface $b, array $options) {
        $b->add('name', 'text', array(
            'label' => 'Name',
        ))->add('patients', 'entity', array(
            'class' => 'AppBundle\Entity\Patient',
            'property' => 'name',
            'multiple' => true,
            'expanded' => true
        ))->add('submit', 'submit');
    }

    public function getName() {
        return 'appbundle_doctor';
    }

}
