<?php

namespace AppBundle\Tests\Controller;

class DoctorControllerTest extends \Symfony\Bundle\FrameworkBundle\Test\WebTestCase {

    public function testSaveDoctor() {
        $client = static::createClient();
        $crawler = $client->request('GET', 'doctor/new');
        
        $this->assertEquals(1, $crawler->filter('h1:contains("Create new doctor")')->count());
        $form = $crawler->selectButton('Submit')->form();
        $form['appbundle_doctor[name]'] = 'John Kowalski';
        $form['appbundle_doctor[patients][0]'] = 1;

        $crawler = $client->submit($form);

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $this->assertSame('application/json', $client->getResponse()->headers->get('Content-Type'));
        $this->assertNotEmpty($client->getResponse()->getContent());
    }

}
