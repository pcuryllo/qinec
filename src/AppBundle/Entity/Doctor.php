<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class Doctor {

    private $id;
    private $name;
    
    /**
     * @var ArrayCollection List of patients
     * @ManyToMany(targetEntity="Patient", inversedBy="doctors")
     */
    private $patients;
    
    public function __construct()
    {
        $this->patients = new ArrayCollection();
    }
    
    /**
     * @return Hospital
     * @param \AppBundle\Entity\Patient $patient
     */
    public function addPatient(Patient $patient) {
        $this->patients[] = $patient;
        return $this;
    }
    
    /**
     * @return Hospital
     * @param \AppBundle\Entity\Patient $patient
     */
    public function removePatient(Patient $patient) {
        $this->patients->removeElement($patient);
        return $this;
    }
    
    /**
     * 
     * @return ArrayCollection Patients's
     */
    public function getPatients() {
        return $this->patients;
    }
    
     /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Hospital
     */
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Hospital
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }
}